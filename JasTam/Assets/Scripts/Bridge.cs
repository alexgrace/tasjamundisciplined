﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bridge : MonoBehaviour {

    public static Color yellowCol = new Color(1, 1, 0, 0.4f);
    public static Color redCol = new Color(1, 0, 0, 0.4f);
    public static Color whiteCol = new Color(1, 1, 1, 0.4f);

    public GameObject outside;

    public bool isCarryingAny;
    public bool isCarrying;
    bool highlighted;
    public bool isAnyHighlighted;

    public string word;

    bool voided;

    int numCollisions;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if(highlighted)
        {
            if (Input.GetMouseButtonDown(0) && !Cursor.visible)
            {
                isCarryingAny = true;
                transform.SetParent(ResourceLocator.Instance.player.transform, false);
                transform.localPosition = new Vector3(0, 0.26f, 0.4f);
                transform.localRotation = Quaternion.identity;
                isCarrying = true;
                GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
                SetHighlighted(false);
            }
        }
        else
        {
            if (isCarrying)
            {
                if (Input.GetMouseButtonDown(0) && !voided)
                {
                    isCarryingAny = false;
                    isCarrying = false;
                    GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
                    transform.parent = null;
                    SetHighlighted(true);

                    var renderers = GetComponentsInChildren<Renderer>();

                    foreach (var rdr in renderers)
                    {
                        rdr.material.color = whiteCol;
                    }

                }

                if(Input.GetKeyDown(KeyCode.Tab))
                {
                    var wordCharArr = word.ToCharArray();

                    for (int i = 0; i < wordCharArr.Length; i++)
                    {
                        GameBehaviour.Instance.AddLetter(wordCharArr[i]);
                    }

                    Destroy(gameObject);
                }
            }
        }        
    }
    
    void SetHighlighted(bool isOn)
    {
        if(isOn && !isAnyHighlighted && !isCarryingAny)
        {
            isAnyHighlighted = true;
            highlighted = true;
            var renderers = GetComponentsInChildren<Renderer>();

            foreach (var rdr in renderers)
            {
                rdr.material.color = yellowCol;
            }
        }
        else
        {
            isAnyHighlighted = false;
            highlighted = false;

            var renderers = GetComponentsInChildren<Renderer>();

            foreach (var rdr in renderers)
            {
                rdr.material.color = whiteCol;
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Player")
            return;

        numCollisions++;

        if (isCarrying)
        {
            voided = true;

            var renderers = GetComponentsInChildren<Renderer>();

            foreach (var rdr in renderers)
            {
                rdr.material.color = redCol;
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.tag == "Player")
            return;

        numCollisions--;

        if (isCarrying && numCollisions == 0)
        {
            voided = false;

            var renderers = GetComponentsInChildren<Renderer>();

            foreach (var rdr in renderers)
            {
                rdr.material.color = whiteCol;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            if(!highlighted && !isCarryingAny)
            {
                SetHighlighted(true);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player" && highlighted && !isCarryingAny)
        {
            SetHighlighted(false);
        }
    }
}
