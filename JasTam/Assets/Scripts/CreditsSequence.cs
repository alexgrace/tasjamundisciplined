﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreditsSequence : MonoBehaviour {

    Image im;
    public GameObject creditsText;
	// Use this for initialization
	void OnEnable () {
        im = GetComponent<Image>();
        StartCoroutine("StartCreditsSeq");
		
	}

    IEnumerator StartCreditsSeq()
    {
        float f = 0.0f;

        while(f < 1.0f)
        {
            f += Time.deltaTime * 0.25f;
            f = f > 1 ? 1 : f;
            im.color = new Color(im.color.r, im.color.g, im.color.b, f);
            yield return null;
        }

        yield return new WaitForSeconds(1.5f);
        creditsText.SetActive(true);

    }
}
