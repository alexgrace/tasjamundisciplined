﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour {

    bool open;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OpenGate()
    {
        if (open)
            return;

        open = true;

        StartCoroutine("die");
    }


    IEnumerator die()
    {
        int ticker = 0;

        while(ticker < 1000)
        {
            ticker++;
            transform.position += new Vector3(2*Time.deltaTime, 0, 0);
            yield return null;
        }

        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider col)
    {
        OpenGate();
    }
}
