﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LetterButtonUIBehaviour : MonoBehaviour {

    public static LetterButtonUIBehaviour Instance;

    [SerializeField] GameObject buttonPanel;

    [SerializeField] Button backButton;
    [SerializeField] Button confirmButton;
    [SerializeField] Text typedWordText;

    List<Button> letterButtons;

    private void Awake()
    {
        Instance = this;
        letterButtons = new List<Button>();
        backButton.onClick.AddListener(() => OnReset());
        confirmButton.onClick.AddListener(() => OnConfirm());
    }

    public void Init()
    {
        typedWordText.text = "";

        foreach(Transform child in buttonPanel.transform)
        {
            Destroy(child.gameObject);
        }

        letterButtons = new List<Button>();
        confirmButton.interactable = false;

        foreach (char l in GameBehaviour.Instance.collectedLetters)
        {
            var button = Instantiate(ResourceLocator.Instance.letterButton).GetComponent<Button>();
            letterButtons.Add(button.GetComponent<Button>());
            button.transform.SetParent(buttonPanel.transform);
            button.transform.GetChild(0).GetComponent<RawImage>().texture = ResourceLocator.Instance.letters[(int)l - 97];
            button.onClick.AddListener(() => 
                {
                    button.interactable = false;
                    typedWordText.text += l;
                    if(GameDictionary.CheckWord(typedWordText.text))
                    {
                        confirmButton.interactable = true;
                    }
                    else
                    {
                        confirmButton.interactable = false;
                    }
                });

            button.transform.localScale = Vector3.one;
        }
    }

    void OnConfirm()
    {
        BridgeBuilder.Instance.MakeBridge(typedWordText.text);

        var wordCharArr = typedWordText.text.ToCharArray();

        for (int i = 0; i < wordCharArr.Length; i++)
        {
            GameBehaviour.Instance.RemoveLetter(wordCharArr[i]);
        }

        GameBehaviour.Instance.ToggleUI();
    }

    void OnReset()
    {
        GameBehaviour.Instance.ToggleUI();
    }
}
