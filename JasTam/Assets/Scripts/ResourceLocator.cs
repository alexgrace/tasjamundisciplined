﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class ResourceLocator : MonoBehaviour {

    public static ResourceLocator Instance { get; private set; }

    public GameObject letterButton;
    public GameObject player;

    [Header("LetterTextures")]
    public Texture[] letters;

    public Material[] mats;

    private void Awake()
    {
        Instance = this;
    }

}
