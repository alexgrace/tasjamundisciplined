﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class GameBehaviour : MonoBehaviour {

    public static GameBehaviour Instance { get; private set; }
    public List<char> collectedLetters;
    bool restrictInput = false;

    [SerializeField] GameObject bgTint;
    [SerializeField] GameObject letterUIButtons;
    [SerializeField] FirstPersonController fpsController;
    [SerializeField] LetterButtonUIBehaviour lbub;


    private void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if(Input.GetKeyDown(KeyCode.Tab))
        {
            ToggleUI();
        }
    }

    public void ToggleUI()
    {
        restrictInput = !restrictInput;
        fpsController.enabled = !restrictInput;
        letterUIButtons.SetActive(restrictInput);
        bgTint.SetActive(restrictInput);
        Cursor.visible = restrictInput;
        Cursor.lockState = restrictInput ? CursorLockMode.None : CursorLockMode.Locked;
        lbub.Init();
    }

    public void AddLetter(char letter)
    {
        collectedLetters.Add(letter);
        lbub.Init();
    }

    public void InitLetterButtonUIBehaviour()
    {
        lbub.Init();
    }

    public void RemoveLetter(char letter)
    {
        collectedLetters.Remove(letter);
    }
}
