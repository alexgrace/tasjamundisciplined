﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class GameDictionary : MonoBehaviour {

    [SerializeField] TextAsset dictFile;

    static List<List<string>> wordDictionary;

    // Use this for initialization
    void Start () {
        wordDictionary = new List<List<string>>();

        for (int i = 0; i <= 10; i++)
        {
            wordDictionary.Add(new List<string>());
        }

        ParseDict();
    }

    void Update()
    {
        /*
        if(Input.GetKeyDown(KeyCode.A))
        {
           // var nineLetters = FindTwoSubAnagrams("coastlines", 5, 4);
            FindLettersForLevel();
        }
        */
    }

    void ParseDict()
    {
        StringReader reader = new StringReader(dictFile.text);

        while(true)
        {
            var aLine = reader.ReadLine();

            if (aLine == null)
                break;
            
            if(aLine.Length < 3 || aLine.Length > 10)
            {
                continue;
            }

            wordDictionary[aLine.Length].Add(aLine);
        }

        print("Dictionary parsed.");
    }
    
    char[] FindLettersForLevel()
    {
        while(true)
        {
            var tenLetters = wordDictionary[10][UnityEngine.Random.Range(0, wordDictionary[10].Count())]; if (FindAnagrams(tenLetters).Count() < 2)  continue;

            var nineLetters = FindTwoSubAnagrams(tenLetters, 5, 4); if (nineLetters == null) if (FindAnagrams(nineLetters).Count() < 3) continue;
            var eightLetters = FindSubAnagram(nineLetters, 8); if (eightLetters == null) if (FindAnagrams(eightLetters).Count() < 3) continue;
            var sevenLetters = FindTwoSubAnagrams(eightLetters, 3, 4); if (sevenLetters == null) if (FindAnagrams(sevenLetters).Count() < 3) continue;
            var sixLetters = FindSubAnagram(sevenLetters, 6); if (sixLetters == null) if (FindAnagrams(sixLetters).Count() < 3) continue;
            // Find two letters here.
            var fourLetters = FindSubAnagram(sixLetters, 4); if (sixLetters == null) if (FindAnagrams(fourLetters).Count() < 3) continue;
            var threeLetters = FindSubAnagram(fourLetters, 3); if (sixLetters == null) if (FindAnagrams(threeLetters).Count() < 3) continue;

            print(tenLetters + " " + nineLetters + " " + eightLetters + " " + sevenLetters + " " + sixLetters + " " + fourLetters + " " + threeLetters);

            List<char> charList = new List<char>();
            charList.Add(tenLetters.ToCharArray().Except(nineLetters.ToCharArray()).ToArray()[0]);
            charList.Add(nineLetters.ToCharArray().Except(eightLetters.ToCharArray()).ToArray()[0]);
            charList.Add(eightLetters.ToCharArray().Except(sevenLetters.ToCharArray()).ToArray()[0]);
            charList.Add(sevenLetters.ToCharArray().Except(sixLetters.ToCharArray()).ToArray()[0]);

            var sixMinusFour = sixLetters.ToCharArray().Except(fourLetters.ToCharArray()).ToArray();

            charList.Add(sixMinusFour[0]);
            charList.Add(sixMinusFour[1]);

            charList.Add(fourLetters.ToCharArray().Except(threeLetters.ToCharArray()).ToArray()[0]);

            charList.Add(threeLetters.ToCharArray()[0]);
            charList.Add(threeLetters.ToCharArray()[1]);
            charList.Add(threeLetters.ToCharArray()[2]);

            print(new string(charList.ToArray()));

            return charList.ToArray();
        }
    }

    string FindTwoSubAnagrams(string word, int firstLength, int secondLength)
    {
        for (int i = 0; i < wordDictionary[firstLength].Count; i++)
        {
            if (IsSubAnagram(wordDictionary[firstLength][i], word))
            {
                var str = wordDictionary[firstLength][i];
                string remainder = new string(word.Except(wordDictionary[firstLength][i]).ToArray());

                for (int j = 0; j < wordDictionary[secondLength].Count; j++)
                {
                    if(IsSubAnagram(wordDictionary[secondLength][j],remainder))
                    {
                        var anagrams = FindAnagrams(wordDictionary[secondLength][j]);

                        if (anagrams.Count == 0)
                            continue;
                        else
                        {
                            return wordDictionary[firstLength][i] + "" + wordDictionary[secondLength][j];
                        }
                    }
                }                
            }
        }

        return null;
    }

    string FindSubAnagram(string word, int length)
    {
        List<string> subAnagrams = new List<string>();

        foreach(string s in wordDictionary[length])
        {
            if (IsSubAnagram(s, word))
            {
                subAnagrams.Add(s);
            }
        }

        return subAnagrams[UnityEngine.Random.Range(0,subAnagrams.Count)];
    }

    List<string> FindAnagrams(string word)
    {
        var anagrams = new List<string>();
        foreach(string s in wordDictionary[word.Length])
        {
            if (s == word)
                continue;

            var wordArr = word.ToCharArray();
            Array.Sort(wordArr);

            var sArr = s.ToCharArray();
            Array.Sort(sArr);

            if (sArr.SequenceEqual(wordArr))
            {
                anagrams.Add(s);
            }
        }

        return anagrams;
    }

    // B is longer than A
    bool IsSubAnagram(string a, string b)
    {
        var charA = a.ToCharArray();
        var charB = b.ToCharArray();

        Array.Sort(charA);
        Array.Sort(charB);

        var indexA = 0;
        var indexB = 0;

        while (indexB < b.Length && indexA < a.Length)
        {
            if(charA[indexA] == charB[indexB])
            {
                indexA++;
            }

            indexB++;
        }

        return indexA == a.Length;
    }

    public static bool CheckWord(string s)
    {
        return wordDictionary[s.Length].Contains(s);
    }


}
