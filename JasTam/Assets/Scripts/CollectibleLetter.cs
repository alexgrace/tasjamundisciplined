﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleLetter : MonoBehaviour {

    public char letter;

    public bool isBridgeObj = false;

    [SerializeField] GameObject letterQuad;
    [SerializeField] GameObject letterQuad2;
    [SerializeField] GameObject letterMesh;

    private void Start()
    {
        UpdateMaterial(letter);
    }

    private void Update()
    {
        if(!isBridgeObj)
        {
            letterMesh.transform.Rotate(new Vector3(0, 180 * Time.deltaTime, 0));
            letterMesh.transform.localPosition = new Vector3(0, 0.1f * Mathf.Sin(Time.time * 4), 0);
        }        
    }

    void UpdateMaterial(char letter)
    {
        var alphabetIndex = (int)letter - 97;
        letterQuad2.GetComponent<Renderer>().material = ResourceLocator.Instance.mats[alphabetIndex];
        letterQuad.GetComponent<Renderer>().material = ResourceLocator.Instance.mats[alphabetIndex];
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            GameBehaviour.Instance.AddLetter(letter);
            Destroy(gameObject);
        }
    }

}
