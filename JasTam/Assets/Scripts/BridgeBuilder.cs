﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeBuilder : MonoBehaviour
{
    public static BridgeBuilder Instance;

    [SerializeField] GameObject bridgeBasePrefab;
    [SerializeField] GameObject bridgeUnit;

    private void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
	}

    public void MakeBridge(string word)
    {
        var wordChars = word.ToCharArray();
        var go = Instantiate(bridgeBasePrefab);
        go.GetComponent<Bridge>().isCarrying = true;
        go.GetComponent<Bridge>().isCarryingAny = true;
        go.GetComponent<Bridge>().word = word;
        go.GetComponent<Bridge>().outside.transform.localScale = new Vector3(1, 1, word.Length);
        var rb = go.GetComponent<Rigidbody>();
        rb.mass = 5000;
        rb.constraints = RigidbodyConstraints.FreezeAll;

        for (int i = 0; i < wordChars.Length; i++)
        {
            var unit = Instantiate(bridgeUnit);
            var collLetter = unit.GetComponent<CollectibleLetter>();
            collLetter.isBridgeObj = true;
            collLetter.letter = wordChars[i];
            unit.transform.position = new Vector3(0, 0, i + 0.5f);
            unit.transform.SetParent(go.transform);
        }

        go.transform.SetParent(ResourceLocator.Instance.player.transform, false);
        go.transform.localPosition = new Vector3(0, 0.26f, 0.4f);
    }
}
